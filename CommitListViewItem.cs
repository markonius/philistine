﻿using System.Linq;
using System.Windows.Forms;

namespace Philistine
{
	public class CommitListViewItem : ListViewItem
	{
		public readonly string commitHash;

		public CommitListViewItem(string[] parts) : base(parts.Skip(1).ToArray())
		{
			commitHash = parts[0];
		}
	}
}
