﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;

namespace Philistine
{
	public partial class MainWindow : MaterialForm
	{
		private readonly CommandDispatcher commandDispatcher;
		private readonly Settings settings = new Settings();

		private readonly LogWindow logWindow = new LogWindow();
		private readonly PushWindow pushWindow;
		private SettingsWindow settingsWindow;
		private CommitHistoryWindow commitHistoryWindow;

		public MainWindow()
		{
			InitializeComponent();

			var materialSkinManager = MaterialSkinManager.Instance;
			materialSkinManager.AddFormToManage(this);
			materialSkinManager.Theme = MaterialSkinManager.Themes.DARK;
			materialSkinManager.ColorScheme = new ColorScheme(
				Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);

			materialSkinManager.UserColors.PrimaryButton = Colors.green;
			materialSkinManager.UserColors.RaisedButton = Colors.slate;
			materialSkinManager.UserColors.PrimaryButtonText = Colors.black;
			materialSkinManager.UserColors.RaisedButtonText = Colors.white;
			materialSkinManager.UserColors.Borders = Colors.white;
			materialSkinManager.UserColors.DisabledColor = Colors.lightSlate;

			changedFiles.TreeViewNodeSorter = new FileNode.Comparer();
			commandDispatcher = new CommandDispatcher(settings, UpdateProgressBar);
			Run("git", "config user.name", onStdOut: name => settings.userName = name);

			pushWindow = new PushWindow(Run, PostPush);

			VerifyRepositoryAndRefresh();
		}

		private void PostPush()
		{
			var unlocks = new List<ITask<bool>>();
			foreach (FileNode fileNode in changedFiles.Nodes)
			{
				if (fileNode.file.lockState == LockState.Locked)
				{
					if (fileNode.file.changeState == ChangeState.Changed)
						unlocks.Add(Run("git", $"lfs unlock {fileNode.GetRelativePath(settings.repositoryPath)}"));
					else if (fileNode.file.changeState == ChangeState.Deleted)
						unlocks.Add(Run("git", $"lfs unlock --force {fileNode.GetRelativePath(settings.repositoryPath)}"));
				}
			}
			unlocks.Bind(_ => RefreshStatus());
		}

		private void VerifyRepositoryAndRefresh()
		{
			Run("git", "status --porcelain").Bind(
				wasError =>
				{
					if (wasError)
						this.Invoke(
							() =>
							{
								ShowSettingsWindow();
								settingsWindow.SetRepositoryIsValid(false);
							});
					else
						RefreshStatus();
				});
		}

		private void LogOutput(string line)
		{
			logWindow.Invoke(() => logWindow.Log($"{line}\r\n"));
		}

		private void UpdateProgressBar(int value)
		{
			this.Invoke(() => progressBar.Value = value);
		}

		private void MainWindow_Resize(object sender, EventArgs e)
		{
			int margin = fileTree.Location.X;
			fileTree.Width = Width / 2 - 2 * margin;
			changedFiles.Left = Width / 2 + margin;
			changedFiles.Width = fileTree.Width;
		}

		private void toolsButton_Click(object sender, EventArgs e)
		{
			hamburgerMenu.Show(Location + (Size)toolsButton.Location + new Size(toolsButton.Width, 0));
		}

		private void refreshButton_Click(object sender, EventArgs e)
		{
			RefreshStatus();
		}

		private void RefreshStatus()
		{
			Run("git", "fetch").Bind(
				__ =>
					RefreshGitStatus().Bind(
						status =>
						{
							fileTree.Invoke(
								() =>
								{
									fileTree.Nodes.Clear();
									changedFiles.Nodes.Clear();
								});
							RefreshFileTree(status);
							AddDeletedFiles(status);
							RefreshLockedFiles().Bind(
								_ =>
								{
									changedFiles.Invoke(
										() =>
										{
											foreach (FileNode fileNode in changedFiles.Nodes)
												fileNode.RefreshIcon();
											changedFiles.Sort();
										});
								});

							pullButton.Invoke(() => pullButton.Enabled = status.behind);
							this.Invoke(() => pushButton.Enabled = pushWindow.PushButtonEnabled = status.Count > 0);
						}));
		}

		private void AddDeletedFiles(GitStatus status)
		{
			foreach (string deletedFile in status.Where(c => c.Item2 == ChangeState.Deleted).Select(c => c.Item1))
			{
				var currentDir = fileTree.Nodes;

				var directory = Directory.GetParent(deletedFile).FullName;
				string relativeDirectory;
				if (directory.Length > settings.repositoryPath.Length)
				{
					relativeDirectory = directory.Replace(settings.repositoryPath + "\\", "");
					var splitDirectory = relativeDirectory.Split('\\');
					for (int i = 0; i < splitDirectory.Length; i++)
					{
						var newDir = currentDir.OfType<DirectoryNode>()
							.FirstOrDefault(d => d.directory.name == splitDirectory[i])?.Nodes;
						if (newDir == null)
						{
							string fullPath = Path.GetFullPath(string.Join("\\", splitDirectory.Take(i)));
							var directoryNode = new DirectoryNode(fullPath);
							TreeNodeCollection closedDir = currentDir;
							fileTree.Invoke(() => closedDir.Add(directoryNode));
							currentDir = directoryNode.Nodes;
						}
						else
						{
							currentDir = newDir;
						}
					}
				}
				fileTree.Invoke(
					() =>
					{
						var fileInfo = new FileInfo(directory, Path.GetFileName(deletedFile))
							{ changeState = ChangeState.Deleted };
						currentDir.Add(new FileNode(fileInfo));
						changedFiles.Nodes.Add(new FileNode(fileInfo));
					});
			}
		}

		private ITask<GitStatus> RefreshGitStatus()
		{
			var status = new GitStatus();
			return Run(
				"git", "status --porcelain -b",
				onStdOut: line =>
				{
					if (line[0] == '#')
					{
						if (Regex.IsMatch(line, "\\[behind \\d+\\]$"))
							status.behind = true;
					}
					else
					{
						string filename = line.Substring(3);
						filename = Path.GetFullPath(settings.repositoryPath + "/" + filename);
						if (line[0] != ' ' && line[0] != '?')
						{
							status.Add((filename, ChangeState.Unknown));
						}
						else
						{
							switch (line[1])
							{
								case ' ':
									status.Add((filename, ChangeState.Unchanged));
									break;
								case 'M':
									status.Add((filename, ChangeState.Changed));
									break;
								case 'A':
								case '?':
									status.Add((filename, ChangeState.Added));
									break;
								case 'D':
									status.Add((filename, ChangeState.Deleted));
									break;
								default:
									status.Add((filename, ChangeState.Unknown));
									break;
							}
						}
					}
				}).Bind(_ => status);
		}

		private ITask<bool> RefreshLockedFiles()
		{
			fileTree.Invoke(
				() =>
				{
					foreach (FileNode node in fileTree.Nodes.OfType<TreeNode>().FilterAndSelectRecursive(
						n => n is FileNode,
						n =>
						{
							if (n is DirectoryNode dn)
								return dn.Nodes.OfType<TreeNode>();
							else
								return Enumerable.Empty<TreeNode>();
						}
					).Cast<FileNode>())
					{
						node.file.lockState = LockState.Unlocked;
						node.RefreshIcon();
					}
				});

			return Run(
				"git", "lfs locks",
				onStdOut: entry =>
				{
					string file = entry.Remove(entry.IndexOf('\t')).TrimEnd(' ');
					string absPath;
					try
					{
						absPath = Path.GetFullPath(settings.repositoryPath + '/' + file);
					}
					catch (NotSupportedException)
					{
						Console.Error.WriteLine("A file is locked with absolute path.");
						return;
					}

					string locker = entry.Substring(entry.IndexOf('\t') + 1);
					locker = locker.Remove(locker.IndexOf('\t'));

					fileTree.Invoke(
						() =>
						{
							var fileNode = (FileNode)fileTree.Nodes.OfType<TreeNode>().FilterAndSelectRecursive(
									n =>
									{
										if (n is FileNode fn)
											return fn.AbsolutePath == absPath;
										else
											return false;
									},
									n =>
									{
										if (n is DirectoryNode dn)
											return dn.Nodes.OfType<TreeNode>();
										else
											return Enumerable.Empty<TreeNode>();
									})
								.FirstOrDefault();
							if (fileNode != null)
							{
								fileNode.file.lockState = locker == settings.userName ? LockState.Locked
									: LockState.LockedBySomeoneElse;
								fileNode.RefreshIcon();
								if (fileNode.file.lockState == LockState.Locked
									&& fileNode.file.changeState == ChangeState.Unchanged)
									AddChangedFile(new FileNode(fileNode.file));
							}
							else if (locker == settings.userName)
							{
								var newFile = new FileInfo(Directory.GetParent(absPath).FullName, Path.GetFileName(absPath))
								{
									changeState = ChangeState.Unchanged,
									lockState = LockState.Locked
								};
								AddChangedFile(new FileNode(newFile));
							}
						});
				});
		}

		private void AddChangedFile(FileNode fileNode)
		{
			fileNode.Text = fileNode.GetRelativePath(settings.repositoryPath);
			for (int i = 0; i < changedFiles.Nodes.Count; i++)
			{
				var currentNode = (FileNode)changedFiles.Nodes[i];
				if (fileNode.Priority > currentNode.Priority)
				{
					changedFiles.Nodes.Insert(i, fileNode);
					return;
				}
			}
			changedFiles.Nodes.Add(fileNode);
		}

		private void RefreshFileTree(GitStatus gitStatus)
		{
			string gitignorePath = Path.Combine(settings.repositoryPath, ".gitignore");
			var rules = File.Exists(gitignorePath) ? File.ReadLines(gitignorePath) : new string[0];
			var gitIgnore = new GitIgnore(new[] { "/.git/" }.Concat(rules));

			string root = settings.repositoryPath;

			foreach (string directory in Directory.EnumerateDirectories(root)
				.Where(d => !gitIgnore.IsIgnoredDirectory(settings.repositoryPath, d))
			)
			{
				var directoryNode = new DirectoryNode(directory);
				fileTree.Invoke(() => fileTree.Nodes.Add(directoryNode));
				PopulateDirectory(directoryNode, gitIgnore, gitStatus);
			}

			foreach (string file in Directory.EnumerateFiles(root)
				.Where(d => !gitIgnore.IsIgnoredFile(settings.repositoryPath, d))
			)
				fileTree.Invoke(
					() =>
					{
						var fileNode = new FileNode(file, gitStatus);
						fileTree.Nodes.Add(fileNode);
						if (fileNode.file.changeState != ChangeState.Unchanged)
							AddChangedFile(new FileNode(fileNode.file));
					});
		}

		private void PopulateDirectory(DirectoryNode directory, GitIgnore ignore, GitStatus gitStatus)
		{
			foreach (string dir in Directory.EnumerateDirectories(directory.AbsolutePath)
				.Where(d => !ignore.IsIgnoredDirectory(settings.repositoryPath, d))
			)
			{
				var childDirectory = new DirectoryNode(dir);
				fileTree.Invoke(() => directory.Nodes.Add(childDirectory));
				PopulateDirectory(childDirectory, ignore, gitStatus);
			}

			foreach (string file in Directory.EnumerateFiles(directory.AbsolutePath)
				.Where(d => !ignore.IsIgnoredFile(settings.repositoryPath, d))
			)
				fileTree.Invoke(
					() =>
					{
						var fileNode = new FileNode(file, gitStatus);
						directory.Nodes.Add(fileNode);
						if (fileNode.file.changeState != ChangeState.Unchanged)
							AddChangedFile(new FileNode(fileNode.file));
					});
		}

		public static string GetTail(string directory) => directory.Substring(directory.LastIndexOf('\\') + 1);

		private void fileTree_MouseClick(object sender, MouseEventArgs e) { }

		private void checkoutToolStripMenuItem_Click(object sender, EventArgs e)
		{
			string path = ((FSNode)fileTree.SelectedNode).AbsolutePath;
			Run("git", $"lfs lock \"{GetRelativePath(settings.repositoryPath, path)}\"");
		}

		private string GetRelativePath(string directory, string path)
		{
			return path.Substring(directory.Length + 1);
		}

		private void checkedOutFiles_MouseClick(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
				changesContextMenu.Show(MousePosition);
		}

		private void unlockFile_Click(object sender, EventArgs e)
		{
			var path = ((FileNode)changedFiles.SelectedNode).AbsolutePath;
			Run("git", $"lfs unlock \"{GetRelativePath(settings.repositoryPath, path)}\"");
		}

		private void fileTree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				fileTree.SelectedNode = e.Node;
				workspaceContextMenu.Show(MousePosition);
			}
		}

		private void logToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ShowLogWindow();
		}

		private void ShowLogWindow()
		{
			logWindow.Show();
			logWindow.BringToFront();
		}

		private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ShowSettingsWindow();
		}

		private void ShowSettingsWindow()
		{
			if (settingsWindow == null)
			{
				settingsWindow = new SettingsWindow(settings, Run, RefreshStatus);
				settingsWindow.Closed += (o, a) => settingsWindow = null;
				settingsWindow.Show();
			}
			else
			{
				settingsWindow.BringToFront();
			}
		}

		private int commandIndex = 0;

		private ITask<bool> Run(
			string command, string arguments,
			Action<string> onStdOut = null, Action<string> onStdErr = null)
		{
			int index = commandIndex++;
			LogOutput($"[{index}]> {command} {arguments}");

			return commandDispatcher.Run(
				command, arguments,
				onStdOut: s =>
				{
					onStdOut?.Invoke(s);
					LogOutput($"[{index}]< {s}");
				},
				onStdErr: s =>
				{
					onStdErr?.Invoke(s);
					LogOutput($"[{index}]! {s}");
				}
			);
		}

		private void pullButton_Click(object sender, EventArgs e)
		{
			List<string> errors = null;
			Run(
				"git", "pull", onStdErr: line =>
				{
					if (errors == null)
						errors = new List<string>();

					errors.Add(line);
				}).Bind(
				error =>
				{
					RefreshStatus();
					if (error)
						MessageBox.Show(
							string.Join("\n", errors), "Pull failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				});
		}

		private void pushButton_Click(object sender, EventArgs e)
		{
			pushWindow.Show();
		}

		private void commitHistoryToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (commitHistoryWindow == null)
			{
				commitHistoryWindow = new CommitHistoryWindow(Run);
				commitHistoryWindow .Closed += (o, a) => commitHistoryWindow = null;
				commitHistoryWindow.Show();
			}
			else
			{
				commitHistoryWindow.BringToFront();
			}
		}
	}

	public delegate ITask<bool> RunCommand(
		string command, string arguments,
		Action<string> onStdOut = null, Action<string> onStdErr = null);
}
