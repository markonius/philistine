﻿using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;

namespace Philistine
{
	public partial class SettingsWindow : MaterialForm
	{
		private readonly Settings settings;
		private readonly RunCommand executor;
		private readonly Action refresh;

		private bool applying;

		public SettingsWindow(Settings settings, RunCommand executor, Action refresh)
		{
			InitializeComponent();
			var materialSkinManager = MaterialSkinManager.Instance;
			materialSkinManager.AddFormToManage(this);

			this.settings = settings;
			this.executor = executor;
			this.refresh = refresh;
			repositoryLocation.Text = settings.repositoryPath;

			versionLabel.Text = Assembly.GetExecutingAssembly().GetName().Version.ToString();
		}

		private void applyButton_Click(object sender, EventArgs e)
		{
			ApplyRepositoryChange();
		}

		private void repositoryLocation_KeyUp(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
			{
				applyButton.AnimateClick(new MouseEventArgs(MouseButtons.Left, 1, applyButton.Width / 2, applyButton.Height / 2, 0));
				applyButton.PerformClick();
			}
		}

		private void ApplyRepositoryChange()
		{
			if (IsAbsolutePath(repositoryLocation.Text) && Directory.Exists(repositoryLocation.Text))
			{
				applying = true;
				var oldPath = settings.repositoryPath;
				settings.repositoryPath = repositoryLocation.Text;

				executor("git", "status").Bind(
					wasError =>
					{
						if (!wasError)
							refresh();
						else
							settings.repositoryPath = oldPath;

						this.Invoke(() => SetRepositoryIsValid(!wasError));
						this.Invoke(RefreshBranchSelectorText);
						settings.Save();
						return applying = false;
					});
			}
			else
			{
				SetRepositoryIsValid(false);
			}
		}

		private bool IsAbsolutePath(string path)
		{
			return path.Length > 2 && Path.IsPathRooted(path) && path[1] == ':';
		}

		public void SetRepositoryIsValid(bool value)
		{
			branchSelector.Enabled = value;
			invalidRepoLabel.Visible = !value;
		}

		private void SettingsWindow_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (e.CloseReason == CloseReason.UserClosing && applying)
				e.Cancel = true;
		}

		private void SettingsWindow_Shown(object sender, EventArgs e)
		{
			branchSelector.Text = "...";
			RefreshBranchSelectorText();
		}

		private void RefreshBranchSelectorText()
		{
			executor(
				"git", "symbolic-ref --short HEAD",
				onStdOut: line => branchSelector.Invoke(() => branchSelector.Text = $"{line} ▼"));
		}

		private void branchSelector_Click(object sender, EventArgs e)
		{
			branchesMenu.Items.Clear();
			executor(
				"git", "branch --format=%(refname:short)",
				onStdOut: line => this.Invoke(
					() => branchesMenu.Items.Add(
						line, null, (o, args) =>
						{
							branchSelector.Text = "...";
							executor("git", $"checkout {line}").Bind(
								_ => RefreshBranchSelectorText());
						}))).Bind(
				_ => this.Invoke(
					() =>
					{
						branchesMenu.Show(Location + (Size)branchSelector.Location + new Size(0, branchSelector.Height));
					}));
		}
	}
}
