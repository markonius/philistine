﻿using System;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;

namespace Philistine
{
	public partial class CommitHistoryWindow : MaterialForm
	{
		private const string GIT_LOG_COMMAND = "log --pretty=\"%H|%s|%an|%cr\"";
		private const string GIT_COMMIT_INFO_COMMAND = "diff-tree -s --pretty=\"%an - %cr%n%n%B\" ";
		private const string GIT_DIFF_COMMAND = "diff --compact-summary ";
		private const string EMPTY_TREE_HASH = "4b825dc642cb6eb9a060e54bf8d69288fbee4904";

		private const int MIN_PANEL_SIZE = 256;

		private readonly RunCommand executor;

		private bool dividerDragging;

		public CommitHistoryWindow(RunCommand executor)
		{
			InitializeComponent();
			var materialSkinManager = MaterialSkinManager.Instance;
			materialSkinManager.AddFormToManage(this);

			this.executor = executor;
			commitList.ColumnWidthChanged += ResizeColumns;
			GetCommits();
		}

		private void ResizeColumns(object sender, ColumnWidthChangedEventArgs columnWidthChangedEventArgs)
		{
			if (commitList.Columns[0].Width + commitList.Columns[1].Width + commitList.Columns[2].Width != commitList.Width)
			{
				int width = commitList.Width - commitList.Columns[1].Width - commitList.Columns[2].Width;
				if (width <= 8)
				{
					if (commitList.Columns[1].Width > 8)
						commitList.Columns[1].Width -= 1;
					if (commitList.Columns[2].Width > 8)
						commitList.Columns[2].Width -= 1;
				}
				else
				{
					commitList.Columns[0].Width = width;
				}
			}
		}

		private void GetCommits()
		{
			executor(
				"git", GIT_LOG_COMMAND, onStdOut: line =>
				{
					var parts = line.Split('|');
					this.Invoke(
						() => commitList.Items.Add(new CommitListViewItem(parts)));
				});
		}

		private void CommitHistoryWindow_Resize(object sender, EventArgs e)
		{
			ResizePanels();
		}

		private void ResizePanels()
		{
			commitList.Width = divider.Left - commitList.Left * 2;
			ResizeColumns(null, null);
			commitInfoPanel.Width = Width - divider.Left - commitList.Left * 2;
			commitInfoPanel.Left = divider.Left + commitList.Left;
		}

		private void divider_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
				dividerDragging = true;
		}

		private void divider_MouseMove(object sender, MouseEventArgs e)
		{
			if (dividerDragging)
			{
				divider.Left = Math.Max(Math.Min(MousePosition.X - Location.X, Width - MIN_PANEL_SIZE), MIN_PANEL_SIZE);
				ResizePanels();
			}
		}

		private void divider_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
				dividerDragging = false;
		}

		private void commitList_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			if (commitList.SelectedItems.Count > 0)
			{
				commitInfo.Text = "";

				var selectedCommit = (CommitListViewItem)commitList.SelectedItems[0];
				string commitHash = selectedCommit.commitHash;
				int selectedCommitIndex = commitList.SelectedIndices[0];
				string previousCommitHash = selectedCommitIndex < commitList.Items.Count - 1 ? $"{commitHash}~" : EMPTY_TREE_HASH;

				void AddText(string line) => this.Invoke(() => commitInfo.AppendText(line + Environment.NewLine));

				executor(
						"git", GIT_COMMIT_INFO_COMMAND + commitHash, onStdOut: AddText)
					.Bind(_ => executor("git", $"{GIT_DIFF_COMMAND} {previousCommitHash} {commitHash}", onStdOut: AddText));
			}
		}
	}
}
