﻿namespace Philistine
{
	partial class CommitHistoryWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.commitList = new MaterialSkin.Controls.MaterialListView();
			this.subject = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.author = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.time = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.divider = new MaterialSkin.Controls.MaterialDivider();
			this.commitInfo = new System.Windows.Forms.TextBox();
			this.commitInfoPanel = new System.Windows.Forms.Panel();
			this.commitInfoPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// commitList
			// 
			this.commitList.AllowColumnReorder = true;
			this.commitList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.commitList.AutoArrange = false;
			this.commitList.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.commitList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.subject,
            this.author,
            this.time});
			this.commitList.Depth = 0;
			this.commitList.FullRowSelect = true;
			this.commitList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.commitList.Location = new System.Drawing.Point(12, 78);
			this.commitList.MouseLocation = new System.Drawing.Point(-1, -1);
			this.commitList.MouseState = MaterialSkin.MouseState.OUT;
			this.commitList.MultiSelect = false;
			this.commitList.Name = "commitList";
			this.commitList.OwnerDraw = true;
			this.commitList.Size = new System.Drawing.Size(358, 390);
			this.commitList.TabIndex = 0;
			this.commitList.UseCompatibleStateImageBehavior = false;
			this.commitList.View = System.Windows.Forms.View.Details;
			this.commitList.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.commitList_ItemSelectionChanged);
			// 
			// subject
			// 
			this.subject.Text = "Subject";
			this.subject.Width = 85;
			// 
			// author
			// 
			this.author.Text = "Author";
			this.author.Width = 121;
			// 
			// time
			// 
			this.time.Text = "Time";
			this.time.Width = 119;
			// 
			// divider
			// 
			this.divider.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
			this.divider.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.divider.Cursor = System.Windows.Forms.Cursors.SizeWE;
			this.divider.Depth = 0;
			this.divider.Location = new System.Drawing.Point(387, 78);
			this.divider.MouseState = MaterialSkin.MouseState.HOVER;
			this.divider.Name = "divider";
			this.divider.Size = new System.Drawing.Size(2, 390);
			this.divider.TabIndex = 1;
			this.divider.MouseDown += new System.Windows.Forms.MouseEventHandler(this.divider_MouseDown);
			this.divider.MouseMove += new System.Windows.Forms.MouseEventHandler(this.divider_MouseMove);
			this.divider.MouseUp += new System.Windows.Forms.MouseEventHandler(this.divider_MouseUp);
			// 
			// commitInfo
			// 
			this.commitInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.commitInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.commitInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.commitInfo.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.commitInfo.ForeColor = System.Drawing.Color.Silver;
			this.commitInfo.Location = new System.Drawing.Point(11, 11);
			this.commitInfo.Multiline = true;
			this.commitInfo.Name = "commitInfo";
			this.commitInfo.ReadOnly = true;
			this.commitInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.commitInfo.Size = new System.Drawing.Size(391, 368);
			this.commitInfo.TabIndex = 2;
			this.commitInfo.Text = "Click on a commit to see its details.";
			this.commitInfo.WordWrap = false;
			// 
			// commitInfoPanel
			// 
			this.commitInfoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.commitInfoPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.commitInfoPanel.Controls.Add(this.commitInfo);
			this.commitInfoPanel.Location = new System.Drawing.Point(395, 78);
			this.commitInfoPanel.Name = "commitInfoPanel";
			this.commitInfoPanel.Padding = new System.Windows.Forms.Padding(8);
			this.commitInfoPanel.Size = new System.Drawing.Size(413, 390);
			this.commitInfoPanel.TabIndex = 3;
			// 
			// CommitHistoryWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(820, 480);
			this.Controls.Add(this.commitInfoPanel);
			this.Controls.Add(this.divider);
			this.Controls.Add(this.commitList);
			this.MinimumSize = new System.Drawing.Size(640, 480);
			this.Name = "CommitHistoryWindow";
			this.Text = "Commit history";
			this.Resize += new System.EventHandler(this.CommitHistoryWindow_Resize);
			this.commitInfoPanel.ResumeLayout(false);
			this.commitInfoPanel.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private MaterialSkin.Controls.MaterialListView commitList;
		private System.Windows.Forms.ColumnHeader subject;
		private System.Windows.Forms.ColumnHeader author;
		private System.Windows.Forms.ColumnHeader time;
		private MaterialSkin.Controls.MaterialDivider divider;
		private System.Windows.Forms.TextBox commitInfo;
		private System.Windows.Forms.Panel commitInfoPanel;
	}
}