﻿namespace Philistine
{
	partial class SettingsWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.repositoryLocation = new MaterialSkin.Controls.MaterialSingleLineTextField();
			this.repositoryLocationLabel = new System.Windows.Forms.Label();
			this.branchSelectorLabel = new System.Windows.Forms.Label();
			this.branchSelector = new MaterialSkin.Controls.MaterialFlatButton();
			this.branchesMenu = new MaterialSkin.Controls.MaterialContextMenuStrip();
			this.applyButton = new MaterialSkin.Controls.MaterialFlatButton();
			this.invalidRepoLabel = new System.Windows.Forms.Label();
			this.versionLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// repositoryLocation
			// 
			this.repositoryLocation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.repositoryLocation.Depth = 0;
			this.repositoryLocation.Hint = "";
			this.repositoryLocation.Location = new System.Drawing.Point(16, 107);
			this.repositoryLocation.MaxLength = 32767;
			this.repositoryLocation.MouseState = MaterialSkin.MouseState.HOVER;
			this.repositoryLocation.Name = "repositoryLocation";
			this.repositoryLocation.PasswordChar = '\0';
			this.repositoryLocation.SelectedText = "";
			this.repositoryLocation.SelectionLength = 0;
			this.repositoryLocation.SelectionStart = 0;
			this.repositoryLocation.Size = new System.Drawing.Size(381, 23);
			this.repositoryLocation.TabIndex = 1;
			this.repositoryLocation.TabStop = false;
			this.repositoryLocation.Text = "A:\\ftw~";
			this.repositoryLocation.UseSystemPasswordChar = false;
			this.repositoryLocation.KeyUp += new System.Windows.Forms.KeyEventHandler(this.repositoryLocation_KeyUp);
			// 
			// repositoryLocationLabel
			// 
			this.repositoryLocationLabel.AutoSize = true;
			this.repositoryLocationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.repositoryLocationLabel.ForeColor = System.Drawing.Color.Silver;
			this.repositoryLocationLabel.Location = new System.Drawing.Point(12, 78);
			this.repositoryLocationLabel.Name = "repositoryLocationLabel";
			this.repositoryLocationLabel.Size = new System.Drawing.Size(129, 17);
			this.repositoryLocationLabel.TabIndex = 3;
			this.repositoryLocationLabel.Text = "Repository location";
			// 
			// branchSelectorLabel
			// 
			this.branchSelectorLabel.AutoSize = true;
			this.branchSelectorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.branchSelectorLabel.ForeColor = System.Drawing.Color.Silver;
			this.branchSelectorLabel.Location = new System.Drawing.Point(12, 167);
			this.branchSelectorLabel.Name = "branchSelectorLabel";
			this.branchSelectorLabel.Size = new System.Drawing.Size(94, 17);
			this.branchSelectorLabel.TabIndex = 4;
			this.branchSelectorLabel.Text = "Active branch";
			// 
			// branchSelector
			// 
			this.branchSelector.AutoSize = true;
			this.branchSelector.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.branchSelector.Depth = 0;
			this.branchSelector.Icon = null;
			this.branchSelector.Location = new System.Drawing.Point(13, 190);
			this.branchSelector.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
			this.branchSelector.MouseState = MaterialSkin.MouseState.HOVER;
			this.branchSelector.Name = "branchSelector";
			this.branchSelector.Primary = false;
			this.branchSelector.Size = new System.Drawing.Size(32, 36);
			this.branchSelector.TabIndex = 5;
			this.branchSelector.Text = "...";
			this.branchSelector.UseVisualStyleBackColor = true;
			this.branchSelector.Click += new System.EventHandler(this.branchSelector_Click);
			// 
			// branchesMenu
			// 
			this.branchesMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.branchesMenu.Depth = 0;
			this.branchesMenu.MouseState = MaterialSkin.MouseState.HOVER;
			this.branchesMenu.Name = "branchesMenu";
			this.branchesMenu.Size = new System.Drawing.Size(61, 4);
			// 
			// applyButton
			// 
			this.applyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.applyButton.AutoSize = true;
			this.applyButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.applyButton.Depth = 0;
			this.applyButton.Icon = null;
			this.applyButton.Location = new System.Drawing.Point(404, 100);
			this.applyButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
			this.applyButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.applyButton.Name = "applyButton";
			this.applyButton.Primary = false;
			this.applyButton.Size = new System.Drawing.Size(63, 36);
			this.applyButton.TabIndex = 6;
			this.applyButton.Text = "Apply";
			this.applyButton.UseVisualStyleBackColor = true;
			this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
			// 
			// invalidRepoLabel
			// 
			this.invalidRepoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.invalidRepoLabel.AutoSize = true;
			this.invalidRepoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.invalidRepoLabel.ForeColor = System.Drawing.Color.Red;
			this.invalidRepoLabel.Location = new System.Drawing.Point(12, 133);
			this.invalidRepoLabel.Name = "invalidRepoLabel";
			this.invalidRepoLabel.Size = new System.Drawing.Size(115, 17);
			this.invalidRepoLabel.TabIndex = 7;
			this.invalidRepoLabel.Text = "Invalid repository";
			this.invalidRepoLabel.Visible = false;
			// 
			// versionLabel
			// 
			this.versionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.versionLabel.AutoSize = true;
			this.versionLabel.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.versionLabel.ForeColor = System.Drawing.Color.Silver;
			this.versionLabel.Location = new System.Drawing.Point(405, 234);
			this.versionLabel.Name = "versionLabel";
			this.versionLabel.Size = new System.Drawing.Size(63, 13);
			this.versionLabel.TabIndex = 8;
			this.versionLabel.Text = "?.?.?.?";
			// 
			// SettingsWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(480, 256);
			this.Controls.Add(this.versionLabel);
			this.Controls.Add(this.invalidRepoLabel);
			this.Controls.Add(this.applyButton);
			this.Controls.Add(this.branchSelector);
			this.Controls.Add(this.branchSelectorLabel);
			this.Controls.Add(this.repositoryLocationLabel);
			this.Controls.Add(this.repositoryLocation);
			this.MaximizeBox = false;
			this.MinimumSize = new System.Drawing.Size(256, 256);
			this.Name = "SettingsWindow";
			this.Sizable = false;
			this.Text = "Settings";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsWindow_FormClosing);
			this.Shown += new System.EventHandler(this.SettingsWindow_Shown);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private MaterialSkin.Controls.MaterialSingleLineTextField repositoryLocation;
		private System.Windows.Forms.Label repositoryLocationLabel;
		private System.Windows.Forms.Label branchSelectorLabel;
		private MaterialSkin.Controls.MaterialFlatButton branchSelector;
		private MaterialSkin.Controls.MaterialContextMenuStrip branchesMenu;
		private MaterialSkin.Controls.MaterialFlatButton applyButton;
		private System.Windows.Forms.Label invalidRepoLabel;
		private System.Windows.Forms.Label versionLabel;
	}
}