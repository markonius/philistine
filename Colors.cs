﻿using System.Drawing;

namespace Philistine
{
	public static class Colors
	{
		public static readonly Color orange = ColorTranslator.FromHtml("#e79535");
		public static readonly Color green = ColorTranslator.FromHtml("#42d2a2");
		public static readonly Color skyBlue = ColorTranslator.FromHtml("#76d2e4");
		public static readonly Color red = ColorTranslator.FromHtml("#e13975");
		public static readonly Color blue = ColorTranslator.FromHtml("#37474f");
		public static readonly Color black = ColorTranslator.FromHtml("#262722");
		public static readonly Color white = ColorTranslator.FromHtml("#e0e0e0");
		public static readonly Color slate = ColorTranslator.FromHtml("#263238");
		public static readonly Color lightSlate = ColorTranslator.FromHtml("#37474f");
	}
}
