﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Philistine
{
	public class CommandDispatcher
	{
		private readonly Settings settings;

		private int runningCommandCount;
		private readonly Action<int> onProgress;

		public event Action<string> OnStdOut;
		public event Action<string> OnStdErr;

		public CommandDispatcher(Settings settings, Action<int> onProgress)
		{
			this.settings = settings;
			this.onProgress = onProgress;
		}

		public ITask<bool> Run(
			string command, string arguments,
			Action<string> onStdOut = null, Action<string> onStdErr = null)
		{
			return Task.Do(
				() =>
				{
					lock (this)
					{
						runningCommandCount++;
						UpdateProgress();
					}

					bool wasError = ExecuteProcess(command, arguments, onStdOut, onStdErr);

					lock (this)
					{
						runningCommandCount--;
						UpdateProgress();
					}

					return wasError;
				});
		}

		private void UpdateProgress()
		{
			onProgress?.Invoke(runningCommandCount);
		}

		private bool ExecuteProcess(string command, string arguments, Action<string> onStdOut, Action<string> onStdErr)
		{
			if (!Directory.Exists(settings.repositoryPath))
			{
				const string invalidRepo = "Repository path is invalid";
				OnStdErr?.Invoke(invalidRepo);
				onStdErr(invalidRepo);
				return true;
			}

			var process = new Process
			{
				StartInfo = new ProcessStartInfo(command, arguments)
				{
					CreateNoWindow = true,
					UseShellExecute = false,
					RedirectStandardOutput = true,
					RedirectStandardError = true,
					WorkingDirectory = settings.repositoryPath,
					StandardOutputEncoding = Encoding.UTF8,
					StandardErrorEncoding = Encoding.UTF8
				}
			};

			process.OutputDataReceived += (_, e) =>
			{
				if (e.Data != null)
				{
					OnStdOut?.Invoke(e.Data);
					onStdOut(e.Data);
				}
			};
			process.ErrorDataReceived += (_, e) =>
			{
				if (e.Data != null)
				{
					OnStdErr?.Invoke(e.Data);
					onStdErr(e.Data);
				}
			};

			process.Start();
			process.BeginOutputReadLine();
			process.BeginErrorReadLine();
			process.WaitForExit();
			bool wasError = process.ExitCode != 0;
			process.Close();
			return wasError;
		}
	}
}
