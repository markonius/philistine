﻿using System.IO;

namespace Philistine
{
	public class FileInfo
	{
		public readonly string name;
		public readonly string containingDirectory;
		public string AbsolutePath => Path.Combine(containingDirectory, name);

		public LockState lockState;
		public ChangeState changeState;

		public FileInfo(string containingDirectory, string name)
		{
			this.containingDirectory = containingDirectory;
			this.name = name;
		}
	}
}
