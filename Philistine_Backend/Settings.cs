﻿using System;
using System.IO;
using Philistine.Auxilaries.JSON;

namespace Philistine
{
	public class Settings
	{
		private static readonly string settingsPath = Path.Combine(
			Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
			"Philistine", "settings.json");

		public string repositoryPath = @"E:\Archive\git\philistine-test";
		public string userName;

		public Settings()
		{
			Load();
		}

		private void Load()
		{
			if (File.Exists(settingsPath))
			{
				var settingsFile = (JSONObject)JSONObject.Read(settingsPath);
				repositoryPath = settingsFile.Get<string>(nameof(repositoryPath));
			}
		}

		public void Save()
		{
			var settingsFile = new JSONObject { [nameof(repositoryPath)] = repositoryPath };
			settingsFile.Write(settingsPath);
		}
	}
}
