﻿using System.Diagnostics.Contracts;
using System.Text;

namespace Philistine.Auxilaries
{
	public static class Aux
	{
		/// <summary>
		/// Returns <see cref="indent"/> tabs.
		/// </summary>
		[Pure]
		public static string Indent(int indent)
		{
			StringBuilder builder = new StringBuilder(indent);
			for (int i = 0; i < indent; i++)
				builder.Append('\t');

			return builder.ToString();
		}

		/// <summary>
		/// Returns <see cref="indent"/> number of concatenated <see cref="indentString"/>.
		/// </summary>
		[Pure]
		public static string Indent(int indent, string indentString)
		{
			StringBuilder builder = new StringBuilder(indent);
			for (int i = 0; i < indent; i++)
				builder.Append(indentString);

			return builder.ToString();
		}

		[Pure]
		public static string Escape(this string str)
		{
			StringBuilder builder = new StringBuilder(str.Length);
			foreach (char c in str)
			{
				switch (c)
				{
					case '"':
					case '\\':
						builder.Append('\\');
						builder.Append(c);
						break;
					case '\t':
						builder.Append("\\t");
						break;
					case '\n':
						builder.Append("\\n");
						break;
					case '\r':
						builder.Append("\\r");
						break;
					default:
						builder.Append(c);
						break;
				}
			}
			return builder.ToString();
		}

		[Pure]
		public static string Deescape(this string str)
		{
			StringBuilder builder = new StringBuilder(str.Length);
			for (int i = 0; i < str.Length; i++)
			{
				char c = str[i];
				if (c == '\\')
				{
					i++;
					c = str[i];
					switch (c)
					{
						case '"':
						case '\\':
							builder.Append(c);
							break;
						case 't':
							builder.Append('\t');
							break;
						case 'n':
							builder.Append('\n');
							break;
						case 'r':
							builder.Append('\r');
							break;
					}
				}
				else
				{
					builder.Append(c);
				}
			}
			return builder.ToString();
		}
	}
}
