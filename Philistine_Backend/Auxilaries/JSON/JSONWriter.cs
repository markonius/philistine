﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;

namespace Philistine.Auxilaries.JSON
{
	public partial class JSONObject
	{
		private static class Writer
		{
			public static void WriteObject(StreamWriter writer, int indent, JSONObject jsonObject)
			{
				writer.Write('\n' + Aux.Indent(indent) + "{\n");
				IEnumerator<KeyValuePair<string, object>> enumerator = jsonObject._pairs.GetEnumerator();
				try
				{
					KeyValuePair<string, object> element;
					if (enumerator.MoveNext())
					{
						element = enumerator.Current;
						WriteElement(writer, indent, element);
					}
					while (enumerator.MoveNext())
					{
						element = enumerator.Current;
						writer.Write(",\n");
						WriteElement(writer, indent, element);
					}
				}
				finally
				{
					enumerator.Dispose();
				}
				writer.Write('\n' + Aux.Indent(indent) + '}');
			}

			private static void WriteElement(StreamWriter writer, int indent, KeyValuePair<string, object> element)
			{
				writer.Write(Aux.Indent(indent + 1) + '"' + element.Key.Escape() + "\": ");

				if (element.Value is JSONObject)
					WriteObject(writer, indent + 1, (JSONObject)element.Value);
				else if (element.Value is IEnumerable && !(element.Value is string))
					WriteArray(writer, indent + 1, (IEnumerable)element.Value);
				else
					writer.Write(Format(element.Value));
			}

			public static void WriteArray(StreamWriter writer, int indent, IEnumerable array)
			{
				writer.Write('\n' + Aux.Indent(indent) + "[\n");

				IEnumerator enumerator = array.GetEnumerator();
				try
				{
					object element;
					if (enumerator.MoveNext())
					{
						element = enumerator.Current;
						WriteArrayElement(writer, indent, element);
					}
					while (enumerator.MoveNext())
					{
						element = enumerator.Current;
						writer.Write(",\n");
						WriteArrayElement(writer, indent, element);
					}
				}
				finally
				{
					IDisposable disposable = enumerator as IDisposable;
					if (disposable != null)
						disposable.Dispose();
				}

				writer.Write('\n' + Aux.Indent(indent) + ']');
			}

			private static void WriteArrayElement(StreamWriter writer, int indent, object element)
			{
				if (element is JSONObject)
					WriteObject(writer, indent + 1, (JSONObject)element);
				else if (element is IEnumerable && !(element is string))
					WriteArray(writer, indent + 1, (IEnumerable)element);
				else
					writer.Write(Aux.Indent(indent + 1) + Format(element));
			}

			[Pure]
			private static string Format(object o)
			{
				if (o == null)
					return "null";
				if (o is bool)
					return o.ToString().ToLower();
				if (o is string)
					return '"' + ((string)o).Escape() + '"';
				return o.ToString();
			}
		}
	}
}
