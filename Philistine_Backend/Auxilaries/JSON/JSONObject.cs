﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Philistine.Auxilaries.JSON
{
	public partial class JSONObject : IEnumerable<KeyValuePair<string, JSONObject>>
	{
		private readonly string defaultValue = "NONE";

		private object _value;

		private Dictionary<string, object> _pairs = new Dictionary<string, object>();

		public Dictionary<string, object>.KeyCollection Keys
		{
			get { return _pairs.Keys; }
		}

		public IEnumerable<object> Values
		{
			get
			{
				IList list = _value as IList;
				if (list != null)
					return list.Cast<object>();

				return _pairs.Values;
			}
		}

		public int Count
		{
			get
			{
				IList list = _value as IList;
				if (list != null)
					return list.Count;
				return _pairs.Count;
			}
		}

		public IEnumerable<KeyValuePair<string, object>> AllPairs
		{
			get { return _pairs; }
		}
		public IEnumerable<JSONObject> Objects
		{
			get
			{
				IList list = _value as IList;
				if (list != null)
					return list.OfType<JSONObject>();

				return _pairs.Where(p => p.Value is JSONObject).Select(p => (JSONObject)p.Value);
			}
		}

		public JSONObject()
		{
			_value = defaultValue;
		}

		public bool ContainsKey(string key)
		{
			return _pairs.ContainsKey(key);
		}

		public void Write(StreamWriter writer)
		{
			Writer.WriteObject(writer, 0, this);
		}

		public void Write(string filename)
		{
			if (filename == null)
				throw new ArgumentNullException(nameof(filename), "Argument is null");
			string directoryName = Path.GetDirectoryName(Path.GetFullPath(filename));
			if (directoryName != null)
				Directory.CreateDirectory(directoryName);
			using (StreamWriter writer = new StreamWriter(filename))
				Writer.WriteObject(writer, 0, this);
		}

		public static void Write(string filename, JSONObject jsonObject)
		{
			using (StreamWriter writer = new StreamWriter(filename))
				Writer.WriteObject(writer, 0, jsonObject);
		}

		public static void Write(string filename, IEnumerable array)
		{
			using (StreamWriter writer = new StreamWriter(filename))
			{
				if (Validate(array))
					Writer.WriteArray(writer, 0, array);
				else
					throw new ArgumentException(array.GetType().Name + " contains invalid JSON values.");
			}
		}

		public static void Write(StreamWriter writer, JSONObject jsonObject)
		{
			Writer.WriteObject(writer, 0, jsonObject);
		}

		public static void Write(StreamWriter writer, IEnumerable array)
		{
			if (Validate(array))
				Writer.WriteArray(writer, 0, array);
			else
				throw new ArgumentException(array.GetType().Name + " contains invalid JSON values.");
		}

		public static IEnumerable Read(StreamReader reader)
		{
			return Reader.ReadRaw(reader);
		}

		public static IEnumerable Read(string filename)
		{
			try
			{
				using (StreamReader reader = new StreamReader(filename))
					return Reader.ReadRaw(reader);
			}
			catch (FormatException)
			{
				Console.Out.WriteLine(Reader.line);
				throw;
			}
		}

		private static bool Validate(object value)
		{
			bool res = value == null
				|| value is sbyte || value is byte || value is short || value is ushort || value is int || value is uint
				|| value is long || value is ulong || value is float || value is double || value is decimal
				|| value is bool || value is string || value is JSONObject;
			if (res) return true;

			if (value is IEnumerable)
			{
				foreach (object o in (IEnumerable)value)
				{
					if (!Validate(o))
						return false;
				}
				return true;
			}

			return false;
		}

		public object Get(string key)
		{
			return _pairs[key];
		}

		/// <summary>
		/// Returns a child object converted to type <see cref="T"/>.
		/// </summary>
		public T Get<T>(string key)
		{
			object val = _pairs[key];
			if (val is T)
				return (T)val;
			return (T)Convert.ChangeType(val, typeof(T));
		}

		public bool TryGet<T>(string key, out T result)
		{
			object val;
			if (!_pairs.TryGetValue(key, out val))
			{
				result = default(T);
				return false;
			}

			if (val is T)
			{
				result = (T)val;
				return true;
			}

			try
			{
				result = (T)Convert.ChangeType(val, typeof(T));
			}
			catch (Exception)
			{
				result = default(T);
				return false;
			}
			return true;
		}

		public void Set(string key, object value)
		{
			if (Validate(value))
				_pairs[key] = value;
			else
				throw new ArgumentException(value.GetType().Name + " is not a valid JSON value.");
		}

		public bool Remove(string key)
		{
			return _pairs.Remove(key);
		}

		public JSONObject this[string key]
		{
			get
			{
				if (!ReferenceEquals(_value, defaultValue))
					throw new InvalidCastException("Tried indexing a JSONObject which represents a " + _value.GetType().Name);

				object val = _pairs[key];
				JSONObject jObj = val as JSONObject;
				return jObj ?? new JSONObject { _value = val };
			}
			set
			{
				if (!ReferenceEquals(_value, defaultValue))
					throw new InvalidCastException("Tried indexing a JSONObject which represents a " + _value.GetType().Name);

				if (!ReferenceEquals(value._value, defaultValue))
					Set(key, value._value);
				else
					Set(key, value);
			}
		}

		public JSONObject this[int index]
		{
			get
			{
				IList list = (IList)_value;
				object val = list[index];
				JSONObject jObj = val as JSONObject;
				return jObj ?? new JSONObject { _value = val };
			}
			set
			{
				IList list = _value as IList;
				if (list == null)
					throw new InvalidCastException("Tried indexing a JSONObject which does not represent an array!");

				if (!ReferenceEquals(value._value, defaultValue))
					list[index] = value._value;
				else
					list[index] = value;
			}
		}

		public static implicit operator JSONObject(ValueType value)
		{
			return new JSONObject { _value = value };
		}

		public static implicit operator JSONObject(string value)
		{
			return new JSONObject { _value = value };
		}

		public static implicit operator JSONObject(Array value)
		{
			return new JSONObject { _value = value };
		}

		public static implicit operator JSONObject(List<object> value)
		{
			return new JSONObject { _value = value };
		}

		public IEnumerator<KeyValuePair<string, JSONObject>> GetEnumerator()
		{
			return _pairs
				.Where(p => p.Value is JSONObject)
				.Select(p => new KeyValuePair<string, JSONObject>(p.Key, (JSONObject)p.Value))
				.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _pairs.GetEnumerator();
		}
	}
}
