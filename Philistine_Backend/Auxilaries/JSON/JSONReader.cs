﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Philistine.Auxilaries.JSON
{
	public partial class JSONObject
	{
		private static class Reader
		{
			private static IEnumerable _root;

			private static StreamReader _reader;
			private static Stack<IReaderState> _stateStack = new Stack<IReaderState>();
			private static char _previousChar;
			public static int line;

			public static IEnumerable ReadRaw(StreamReader reader)
			{
				_reader = reader;
				line = 0;
				_stateStack.Clear();
				_stateStack.Push(new Base());
				_stateStack.Peek().Read();
				return _root;
			}

			private static char ReadChar()
			{
				int k = _reader.Read();
				if (k == -1)
					throw new FormatException("Malformed JSON file " + line);

				_previousChar = (char)k;
				if (_previousChar == '\n')
					line++;
				return (char)k;
			}

			private interface IReaderState
			{
				void Read();
			}

			private struct Base : IReaderState
			{
				public void Read()
				{
					while (true)
					{
						char c = ReadChar();
						switch (c)
						{
							case '\n':
							case '\r':
							case '\t':
							case ' ':
								break;
							case '{':
								_root = new JSONObject();
								_stateStack.Push(new Object((JSONObject)_root));
								_stateStack.Peek().Read();
								return;
							case '[':
								_root = new List<object>();
								_stateStack.Push(new Array((List<object>)_root));
								_stateStack.Peek().Read();
								return;
							default:
								throw new FormatException("Malformed JSON file " + line);
						}
					}
				}
			}

			private struct Object : IReaderState
			{
				private JSONObject _reference;
				public Object(JSONObject reference) { _reference = reference; }

				public void Read()
				{
					while (true)
					{
						if (_previousChar == '}')
						{
							_previousChar = ' ';
							_stateStack.Pop();
							while (true)
							{
								int i = _reader.Read();
								if (i == -1)
									return;
								char h = (char)i;
								if (h == ',' || h == '}' || h == ']')
								{
									_previousChar = h;
									break;
								}
							}
							return;
						}
						char c = ReadChar();
						switch (c)
						{
							case '\n':
							case '\r':
							case '\t':
							case ' ':
							case '}':
								break;
							case '"':
								_stateStack.Push(new Key(_reference));
								_stateStack.Peek().Read();
								break;
							default:
								throw new FormatException("Malformed JSON file " + line);
						}
					}
				}
			}

			private struct Key : IReaderState
			{
				private JSONObject _reference;
				public Key(JSONObject reference) { _reference = reference; }

				public void Read()
				{
					string key = "";
					while (true)
					{
						char c = ReadChar();
						if (c == '"')
							break;
						key += c;
					}
					while (true)
					{
						char c = ReadChar();
						switch (c)
						{
							case '\n':
							case '\r':
							case '\t':
							case ' ':
								break;
							case ':':
								_stateStack.Pop();
								_stateStack.Push(new ObjectValue(_reference, key));
								_stateStack.Peek().Read();
								return;
							default:
								throw new FormatException("Malformed JSON file " + line);
						}
					}
				}
			}

			private struct ObjectValue : IReaderState
			{
				private JSONObject _reference;
				private string _key;
				private string _valueString;

				public ObjectValue(JSONObject reference, string key)
				{
					_reference = reference;
					_key = key;
					_valueString = "";
				}

				public void Read()
				{
					while (true)
					{
						char c = ReadChar();
						if (c == '\n' || c == '\t' || c == '\r' || c == ' ')
							continue;
						if (c == '"')
						{
							ReadString();
							_reference.Set(_key, _valueString);
							return;
						}
						if (c == '+' || c == '-' || char.IsNumber(c))
						{
							_valueString += c;
							ReadNumber();
							return;
						}
						if (char.IsLetter(c))
						{
							_valueString += c;
							ReadWord();
							return;
						}
						if (c == '{')
						{
							JSONObject obj = new JSONObject();
							_reference.Set(_key, obj);
							_stateStack.Push(new Object(obj));
							_stateStack.Peek().Read();
							return;
						}
						if (c == '[')
						{
							List<object> list = new List<object>();
							_reference.Set(_key, list);
							_stateStack.Push(new Array(list));
							_stateStack.Peek().Read();
							return;
						}
						throw new FormatException("Malformed JSON file " + line);
					}
				}

				private void ReadWord()
				{
					while (true)
					{
						char c = ReadChar();
						if (char.IsLetter(c))
							_valueString += c;
						else if (c == ',' || c == '}')
						{
							_stateStack.Pop();
							break;
						}
					}

					if (_valueString == "null")
						_reference.Set(_key, null);
					else
						_reference.Set(_key, bool.Parse(_valueString));
				}

				private void ReadNumber()
				{
					while (true)
					{
						char c = ReadChar();
						if (c == '+' || c == '-' || c == '.' || char.IsLetterOrDigit(c))
							_valueString += c;
						else if (c == ',' || c == '}')
						{
							_stateStack.Pop();
							break;
						}
					}
					_reference.Set(_key, double.Parse(_valueString));
				}

				private void ReadString()
				{
					bool escape = false;
					while (true)
					{
						char c = ReadChar();
						if (c == '"')
							break;
						if (c == '\\')
							escape = true;
						else if (escape)
						{
							switch (c)
							{
								case 'n':
									_valueString += '\n';
									break;
								case 'r':
									_valueString += '\r';
									break;
								case 't':
									_valueString += '\t';
									break;
								case '\\':
									_valueString += '\\';
									break;
								default:
									_valueString += "\\" + c;
									break;
							}
							escape = false;
						}
						else
						{
							_valueString += c;
						}
					}
					while (true)
					{
						char c = ReadChar();
						if (c == ',' || c == '}')
						{
							_stateStack.Pop();
							break;
						}
					}
				}
			}

			private struct Array : IReaderState
			{
				private List<object> _reference;
				public Array(List<object> reference) { _reference = reference; }

				public void Read()
				{
					while (true)
					{
						if (_previousChar == ']')
						{
							_previousChar = ' ';
							_stateStack.Pop();
							while (true)
							{
								int i = _reader.Read();
								if (i == -1)
									return;
								char h = (char)i;
								if (h == ',' || h == '}' || h == ']')
								{
									_previousChar = h;
									break;
								}
							}
							return;
						}
						_stateStack.Push(new ArrayValue(_reference));
						_stateStack.Peek().Read();
					}
				}
			}

			private struct ArrayValue : IReaderState
			{
				private List<object> _reference;
				private string _valueString;

				public ArrayValue(List<object> reference)
				{
					_reference = reference;
					_valueString = "";
				}

				public void Read()
				{
					while (true)
					{
						char c = ReadChar();
						if (c == '\n' || c == '\r' || c == '\t' || c == ' ')
							continue;
						if (c == '"')
						{
							ReadString();
							return;
						}
						if (c == '+' || c == '-' || char.IsNumber(c))
						{
							_valueString += c;
							ReadNumber();
							return;
						}
						if (char.IsLetter(c))
						{
							_valueString += c;
							ReadWord();
							return;
						}
						if (c == '{')
						{
							JSONObject obj = new JSONObject();
							_reference.Add(obj);
							_stateStack.Push(new Object(obj));
							_stateStack.Peek().Read();
							return;
						}
						if (c == '[')
						{
							List<object> list = new List<object>();
							_reference.Add(list);
							_stateStack.Push(new Array(list));
							_stateStack.Peek().Read();
							return;
						}
						if (c == ']')
						{
							return;
						}
						throw new FormatException("Malformed JSON file " + line);
					}
				}

				private void ReadWord()
				{
					bool next = false;
					while (true)
					{
						char c = ReadChar();
						if (char.IsLetter(c))
							_valueString += c;
						else if (c == ',')
						{
							_stateStack.Pop();
							next = true;
							break;
						}
						else if (c == ']')
						{
							_stateStack.Pop();
							break;
						}
					}

					if (_valueString == "null")
						_reference.Add(null);
					else
						_reference.Add(bool.Parse(_valueString));

					if (next)
					{
						_stateStack.Push(new ArrayValue(_reference));
						_stateStack.Peek().Read();
					}
				}

				private void ReadNumber()
				{
					bool next = false;
					bool dec = false;
					while (true)
					{
						char c = ReadChar();
						if (c == '.')
						{
							dec = true;
							_valueString += c;
						}
						if (c == '+' || c == '-' || char.IsLetterOrDigit(c))
							_valueString += c;
						else if (c == ',')
						{
							_stateStack.Pop();
							next = true;
							break;
						}
						else if (c == ']')
						{
							_stateStack.Pop();
							break;
						}
					}

					if (dec)
						_reference.Add(double.Parse(_valueString));
					else
					{
						_reference.Add(int.Parse(_valueString));
					}
					if (next)
					{
						_stateStack.Push(new ArrayValue(_reference));
						_stateStack.Peek().Read();
					}
				}

				private void ReadString()
				{
					bool next = false;
					bool escape = false;
					while (true)
					{
						char c = ReadChar();
						if (escape)
						{
							switch (c)
							{
								case '"':
									_valueString += '"';
									break;
								case 'n':
									_valueString += '\n';
									break;
								case 'r':
									_valueString += '\r';
									break;
								case 't':
									_valueString += '\t';
									break;
								case '\\':
									_valueString += '\\';
									break;
								default:
									_valueString += "\\" + c;
									break;
							}
							escape = false;
						}
						else if (c == '"')
							break;
						else if (c == '\\')
							escape = true;
						else
							_valueString += c;
					}
					while (true)
					{
						char c = ReadChar();
						if (c == ',')
						{
							_stateStack.Pop();
							next = true;
							break;
						}
						if (c == ']')
						{
							_stateStack.Pop();
							break;
						}
					}
					_reference.Add(_valueString);
					if (next)
					{
						_stateStack.Push(new ArrayValue(_reference));
						_stateStack.Peek().Read();
					}
				}
			}
		}
	}
}
