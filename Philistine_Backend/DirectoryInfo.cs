﻿using System.IO;

namespace Philistine
{
	public class DirectoryInfo
	{
		public readonly string name;
		public readonly string containingDirectory;
		public string AbsolutePath => Path.Combine(containingDirectory, name);

		public DirectoryInfo(string containingDirectory, string name)
		{
			this.containingDirectory = containingDirectory;
			this.name = name;
		}
	}
}
