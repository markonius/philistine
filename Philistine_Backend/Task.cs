﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Philistine
{
	public static class Task
	{
		public static ITask<T> Unit<T>(T value) => new UnitTask<T>(value);
		public static ITask<T> Do<T>(Func<T> computation) => new RootTask<T>(computation);

		public static void Bind<TIn>(this IList<ITask<TIn>> taskList, Action<IList<TIn>> function)
		{
			if (taskList.Count == 0)
			{
				function(new List<TIn>());
			}
			else
			{
				var input = new List<TIn>(taskList.Count);
				foreach (var task in taskList)
				{
					task.Bind(
						r =>
						{
							input.Add(r);
							if (input.Count == taskList.Count)
								function(input);
						});
				}
			}
		}
	}

	public interface ITaskIn<in TIn>
	{
		void Run(TIn parameter);
		void Invoke(TIn parameter);
	}

	public interface ITask<out TOut>
	{
		bool IsDone { get; }
		ITask<TOtherOut> Bind<TOtherOut>(Func<TOut, ITask<TOtherOut>> function);
		ITask<TOtherOut> Bind<TOtherOut>(Func<TOut, TOtherOut> function);
		void Bind(Action<TOut> action);
		TOut Wait();
	}

	public class UnitTask<TOut> : ITask<TOut>
	{
		private readonly TOut result;
		public bool IsDone => true;

		public UnitTask(TOut value)
		{
			result = value;
		}

		public ITask<TOtherOut> Bind<TOtherOut>(Func<TOut, ITask<TOtherOut>> function)
		{
			return function(result);
		}

		public ITask<TOtherOut> Bind<TOtherOut>(Func<TOut, TOtherOut> function)
		{
			return Task.Do(() => function(result));
		}

		public void Bind(Action<TOut> action)
		{
			ThreadPool.QueueUserWorkItem(_ => action(result));
		}

		public TOut Wait() => result;
	}

	public class RootTask<TOut> : ITask<TOut>
	{
		protected TOut result;
		private List<ITaskIn<TOut>> nextTasks;
		public bool IsDone { get; private set; }

		protected RootTask() { }

		public RootTask(Func<TOut> computation)
		{
			ThreadPool.QueueUserWorkItem(
				_ =>
				{
					result = computation();
					Continue();
				});
		}

		protected void Continue()
		{
			lock (this)
				IsDone = true;
			if (nextTasks != null)
			{
				foreach (var taskIn in nextTasks.Skip(1))
					taskIn.Run(result);
				nextTasks.First().Invoke(result);
			}
		}

		public ITask<TOtherOut> Bind<TOtherOut>(Func<TOut, ITask<TOtherOut>> function)
		{
			Monitor.Enter(this);
			if (IsDone)
			{
				Monitor.Exit(this);
				return function(result);
			}
			else
			{
				var continuation = ContinueWith(function);
				Monitor.Exit(this);
				return continuation;
			}
		}

		public ITask<TOtherOut> Bind<TOtherOut>(Func<TOut, TOtherOut> function)
		{
			Monitor.Enter(this);
			if (IsDone)
			{
				Monitor.Exit(this);
				return Task.Do(() => function(result));
			}
			else
			{
				var continuation = ContinueWith(function);
				Monitor.Exit(this);
				return continuation;
			}
		}

		public void Bind(Action<TOut> action)
		{
			Monitor.Enter(this);
			if (IsDone)
			{
				Monitor.Exit(this);
				ThreadPool.QueueUserWorkItem(_ => action(result));
			}
			else
			{
				ContinueWith(action);
				Monitor.Exit(this);
			}
		}

		private ITask<TOtherOut> ContinueWith<TOtherOut>(Func<TOut, ITask<TOtherOut>> function)
		{
			TOtherOut Unwrapped(TOut tIn) => function(tIn).Wait();
			var task = new Task<TOut, TOtherOut>(Unwrapped);
			ContinueWithTask(task);
			return task;
		}

		private ITask<TOtherOut> ContinueWith<TOtherOut>(Func<TOut, TOtherOut> function)
		{
			var task = new Task<TOut, TOtherOut>(function);
			ContinueWithTask(task);
			return task;
		}

		private void ContinueWith(Action<TOut> action)
		{
			var task = new EndTask<TOut>(action);
			if (nextTasks == null)
				nextTasks = new List<ITaskIn<TOut>>();
			nextTasks.Add(task);
		}

		private void ContinueWithTask(ITaskIn<TOut> task)
		{
			if (nextTasks == null)
				nextTasks = new List<ITaskIn<TOut>>();
			nextTasks.Add(task);
		}

		public TOut Wait()
		{
			while (!IsDone)
				Thread.Sleep(10);
			return result;
		}
	}

	public class EndTask<TIn> : ITaskIn<TIn>
	{
		private readonly Action<TIn> action;

		public EndTask(Action<TIn> action)
		{
			this.action = action;
		}

		public void Run(TIn parameter)
		{
			ThreadPool.QueueUserWorkItem(_ => action(parameter));
		}

		public void Invoke(TIn parameter)
		{
			action(parameter);
		}
	}

	public class Task<TIn, TOut> : RootTask<TOut>, ITaskIn<TIn>
	{
		private readonly Func<TIn, TOut> function;

		public Task(Func<TIn, TOut> function)
		{
			this.function = function;
		}

		public void Run(TIn parameter)
		{
			ThreadPool.QueueUserWorkItem(
				_ =>
				{
					result = function(parameter);
					Continue();
				});
		}

		public void Invoke(TIn parameter)
		{
			result = function(parameter);
			Continue();
		}
	}
}
