﻿namespace Philistine
{
	public enum LockState
	{
		Unlocked,
		Locked,
		LockedBySomeoneElse,
	}

	public enum ChangeState
	{
		Unchanged,
		Changed,
		Added,
		Deleted,
		Unknown,
	}
}
