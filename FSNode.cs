﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Philistine
{
	public abstract class FSNode : TreeNode
	{
		public abstract string AbsolutePath { get; }

		protected FSNode(string absolutePath)
		{
			Text = MainWindow.GetTail(absolutePath);
		}

		public string GetRelativePath(string relativeTo)
		{
			var pathUri = new Uri(AbsolutePath);
			// Folders must end in a slash
			if (!relativeTo.EndsWith(Path.DirectorySeparatorChar.ToString()))
				relativeTo += Path.DirectorySeparatorChar;
			var folderUri = new Uri(relativeTo);
			return Uri.UnescapeDataString(folderUri.MakeRelativeUri(pathUri).ToString());
		}
	}

	public class FileNode : FSNode
	{
		public readonly FileInfo file;
		public override string AbsolutePath => file.AbsolutePath;
		public int Priority
		{
			get
			{
				if (file.changeState == ChangeState.Added)
					 return 0;
				else if ((file.changeState == ChangeState.Changed || file.changeState == ChangeState.Deleted)
					&& (file.lockState == LockState.Unlocked || file.lockState == LockState.LockedBySomeoneElse))
					return 2;
				else if (file.changeState == ChangeState.Unchanged && file.lockState == LockState.Locked)
					return 1;
				else
					return 0;
			}
		}

		public FileNode(string absolutePath, GitStatus gitStatus) : base(absolutePath)
		{
			file = new FileInfo(Directory.GetParent(absolutePath).FullName, Path.GetFileName(absolutePath));
			(string, ChangeState) thisStatus = default((string, ChangeState));

			if (gitStatus.Count > 0)
			{
				// Git status can contain untracked directories
				var shortestPathLength = gitStatus.Min(s => s.Item1.Length);
				while (absolutePath.Length >= shortestPathLength && thisStatus.Equals(default((string, ChangeState))))
				{
					thisStatus = gitStatus.FirstOrDefault(t => t.Item1 == absolutePath);
					absolutePath = Directory.GetParent(absolutePath.TrimEnd('\\')).FullName + "\\";
				}
			}

			if (!thisStatus.Equals(default((string, ChangeState))))
				file.changeState = thisStatus.Item2;

			RefreshIcon();
		}

		public FileNode(FileInfo file) : base(file.AbsolutePath)
		{
			this.file = file;
			RefreshIcon();
		}

		public void RefreshIcon()
		{
			if (file.changeState == ChangeState.Unknown)
			{
				ImageKey = SelectedImageKey = "Unknown.png";
			}
			else if (file.changeState == ChangeState.Added)
			{
				ImageKey = SelectedImageKey = "Added.png";
			}
			else if (file.changeState == ChangeState.Deleted)
			{
				if (file.lockState == LockState.Locked)
					ImageKey = SelectedImageKey = "LockedAndDeleted.png";
				else if (file.lockState == LockState.LockedBySomeoneElse)
					ImageKey = SelectedImageKey = "DeletedButLockedBySomeoneElse.png";
				else
					ImageKey = SelectedImageKey = "DeletedButNotLocked.png";
			}
			else if (file.changeState == ChangeState.Changed)
			{
				if (file.lockState == LockState.Locked)
					ImageKey = SelectedImageKey = "LockedAndChanged.png";
				else if (file.lockState == LockState.LockedBySomeoneElse)
					ImageKey = SelectedImageKey = "ChangedButLockedBySomeoneElse.png";
				else
					ImageKey = SelectedImageKey = "ChangedButNotLocked.png";
			}
			else if (file.changeState == ChangeState.Unchanged)
			{
				if (file.lockState == LockState.Locked)
					ImageKey = SelectedImageKey = "Locked.png";
				else if (file.lockState == LockState.LockedBySomeoneElse)
					ImageKey = SelectedImageKey = "LockedBySomeoneElse.png";
				else
					ImageKey = SelectedImageKey = "RegularFile.png";
			}
		}

		public class Comparer : System.Collections.IComparer
		{
			public int Compare(object left, object right)
			{
				return -((FileNode)left).Priority.CompareTo(((FileNode)right).Priority);
			}
		}
	}

	public class DirectoryNode : FSNode
	{
		public readonly DirectoryInfo directory;
		public override string AbsolutePath => directory.AbsolutePath;

		public DirectoryNode(string absolutePath) : base(absolutePath)
		{
			directory = new DirectoryInfo(Directory.GetParent(absolutePath).FullName, MainWindow.GetTail(absolutePath));
			ImageKey = SelectedImageKey = "Directory.png";
		}
	}
}
