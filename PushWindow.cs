﻿using System;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;

namespace Philistine
{
	public partial class PushWindow : MaterialForm
	{
		private const string SAMPLE_COMMIT_MESSAGE = @"Short (50 characters or less) summary of changes

More detailed explanatory text, if necessary. Use this to describe your changes in detail.
Say why you've done what you've done, what tasks you've completed or what issues
you've resolved.

Further paragraphs come after blank lines.

- Bullet points are okay, too
- A hyphen or asterisk is used for the bullet.
";
		private readonly RunCommand executor;
		private readonly Action postPush;

		public string CommitMessage
		{
			get => commitMessageBox.Text;
			set => commitMessageBox.Text = value;
		}
		public bool PushButtonEnabled
		{
			get => pushButton.Enabled;
			set => pushButton.Enabled = value;
		}

		public PushWindow(RunCommand executor, Action postPush)
		{
			InitializeComponent();
			var materialSkinManager = MaterialSkinManager.Instance;
			materialSkinManager.AddFormToManage(this);

			this.executor = executor;
			this.postPush = postPush;
		}

		private void pushButton_Click(object sender, EventArgs e)
		{
			if (CommitMessage.Length > 0 && CommitMessage != SAMPLE_COMMIT_MESSAGE)
			{
				executor("git", "add *").Bind(
					error =>
					{
						if (!error)
							executor("git", $"commit -m \"{CommitMessage.Replace("\"", "\\\"")}\"").Bind(
								error2 =>
								{
									if (!error2)
										executor("git", "push").Bind(
											_ =>
											{
												this.Invoke(() => CommitMessage = "");
												postPush();
											});
								});
					});
				Hide();
			}
		}

		private void PushWindow_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (e.CloseReason == CloseReason.UserClosing)
			{
				e.Cancel = true;
				Hide();
			}
		}

		private void helpButton_Click(object sender, EventArgs e)
		{
			CommitMessage += SAMPLE_COMMIT_MESSAGE;
		}
	}
}
