﻿using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;

namespace Philistine
{
	public partial class LogWindow : MaterialForm
	{
		public LogWindow()
		{
			InitializeComponent();

			var materialSkinManager = MaterialSkinManager.Instance;
			materialSkinManager.AddFormToManage(this);

			CreateHandle();
		}

		protected sealed override void CreateHandle()
		{
			base.CreateHandle();
		}

		private void LogWindow_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (e.CloseReason == CloseReason.UserClosing)
			{
				e.Cancel = true;
				Hide();
			}
		}

		public void Log(string text)
		{
			logText.AppendText(text);
		}
	}
}
