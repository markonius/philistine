﻿namespace Philistine
{
	partial class MainWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
			System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Node0");
			System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Node2");
			System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Node1", new System.Windows.Forms.TreeNode[] {
            treeNode2});
			this.pullButton = new MaterialSkin.Controls.MaterialRaisedButton();
			this.pushButton = new MaterialSkin.Controls.MaterialRaisedButton();
			this.fileTreeLabel = new MaterialSkin.Controls.MaterialLabel();
			this.divider = new MaterialSkin.Controls.MaterialDivider();
			this.changesetLabel = new MaterialSkin.Controls.MaterialLabel();
			this.fileTree = new System.Windows.Forms.TreeView();
			this.fileTreeIcons = new System.Windows.Forms.ImageList(this.components);
			this.toolsButton = new MaterialSkin.Controls.MaterialRaisedButton();
			this.hamburgerMenu = new MaterialSkin.Controls.MaterialContextMenuStrip();
			this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.logToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshButton = new MaterialSkin.Controls.MaterialRaisedButton();
			this.workspaceContextMenu = new MaterialSkin.Controls.MaterialContextMenuStrip();
			this.checkoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.changesContextMenu = new MaterialSkin.Controls.MaterialContextMenuStrip();
			this.unlockFile = new System.Windows.Forms.ToolStripMenuItem();
			this.progressBar = new MaterialSkin.Controls.MaterialProgressBar();
			this.changedFiles = new System.Windows.Forms.TreeView();
			this.commitHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.hamburgerMenu.SuspendLayout();
			this.workspaceContextMenu.SuspendLayout();
			this.changesContextMenu.SuspendLayout();
			this.SuspendLayout();
			// 
			// pullButton
			// 
			this.pullButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.pullButton.AutoSize = true;
			this.pullButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.pullButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(130)))), ((int)(((byte)(0)))));
			this.pullButton.Depth = 0;
			this.pullButton.ForeColor = System.Drawing.SystemColors.ControlText;
			this.pullButton.Icon = ((System.Drawing.Image)(resources.GetObject("pullButton.Icon")));
			this.pullButton.Location = new System.Drawing.Point(299, 26);
			this.pullButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.pullButton.Name = "pullButton";
			this.pullButton.Primary = false;
			this.pullButton.Size = new System.Drawing.Size(82, 36);
			this.pullButton.TabIndex = 0;
			this.pullButton.Text = "Pull";
			this.pullButton.UseVisualStyleBackColor = false;
			this.pullButton.Click += new System.EventHandler(this.pullButton_Click);
			// 
			// pushButton
			// 
			this.pushButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.pushButton.AutoSize = true;
			this.pushButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.pushButton.BackColor = System.Drawing.Color.Transparent;
			this.pushButton.Depth = 0;
			this.pushButton.Icon = ((System.Drawing.Image)(resources.GetObject("pushButton.Icon")));
			this.pushButton.Location = new System.Drawing.Point(387, 26);
			this.pushButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.pushButton.Name = "pushButton";
			this.pushButton.Primary = false;
			this.pushButton.Size = new System.Drawing.Size(85, 36);
			this.pushButton.TabIndex = 1;
			this.pushButton.Text = "Push";
			this.pushButton.UseVisualStyleBackColor = false;
			this.pushButton.Click += new System.EventHandler(this.pushButton_Click);
			// 
			// fileTreeLabel
			// 
			this.fileTreeLabel.AutoSize = true;
			this.fileTreeLabel.Depth = 0;
			this.fileTreeLabel.Font = new System.Drawing.Font("Roboto", 11F);
			this.fileTreeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.fileTreeLabel.Location = new System.Drawing.Point(12, 71);
			this.fileTreeLabel.MouseState = MaterialSkin.MouseState.HOVER;
			this.fileTreeLabel.Name = "fileTreeLabel";
			this.fileTreeLabel.Size = new System.Drawing.Size(84, 19);
			this.fileTreeLabel.TabIndex = 3;
			this.fileTreeLabel.Text = "Workspace";
			// 
			// divider
			// 
			this.divider.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
			this.divider.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.divider.Depth = 0;
			this.divider.Location = new System.Drawing.Point(383, 71);
			this.divider.MouseState = MaterialSkin.MouseState.HOVER;
			this.divider.Name = "divider";
			this.divider.Size = new System.Drawing.Size(2, 407);
			this.divider.TabIndex = 4;
			this.divider.TabStop = false;
			this.divider.Text = "divider";
			// 
			// changesetLabel
			// 
			this.changesetLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.changesetLabel.AutoSize = true;
			this.changesetLabel.Depth = 0;
			this.changesetLabel.Font = new System.Drawing.Font("Roboto", 11F);
			this.changesetLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.changesetLabel.Location = new System.Drawing.Point(409, 71);
			this.changesetLabel.MouseState = MaterialSkin.MouseState.HOVER;
			this.changesetLabel.Name = "changesetLabel";
			this.changesetLabel.Size = new System.Drawing.Size(67, 19);
			this.changesetLabel.TabIndex = 5;
			this.changesetLabel.Text = "Changes";
			// 
			// fileTree
			// 
			this.fileTree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.fileTree.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.fileTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.fileTree.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.fileTree.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.fileTree.FullRowSelect = true;
			this.fileTree.ImageKey = "RegularFile.png";
			this.fileTree.ImageList = this.fileTreeIcons;
			this.fileTree.ItemHeight = 28;
			this.fileTree.Location = new System.Drawing.Point(12, 93);
			this.fileTree.Name = "fileTree";
			treeNode1.Name = "Node0";
			treeNode1.Text = "Node0";
			treeNode2.Name = "Node2";
			treeNode2.Text = "Node2";
			treeNode3.Name = "Node1";
			treeNode3.Text = "Node1";
			this.fileTree.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode3});
			this.fileTree.SelectedImageIndex = 0;
			this.fileTree.Size = new System.Drawing.Size(367, 385);
			this.fileTree.TabIndex = 7;
			this.fileTree.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.fileTree_NodeMouseClick);
			this.fileTree.MouseClick += new System.Windows.Forms.MouseEventHandler(this.fileTree_MouseClick);
			// 
			// fileTreeIcons
			// 
			this.fileTreeIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("fileTreeIcons.ImageStream")));
			this.fileTreeIcons.TransparentColor = System.Drawing.Color.Transparent;
			this.fileTreeIcons.Images.SetKeyName(0, "Directory.png");
			this.fileTreeIcons.Images.SetKeyName(1, "Unknown.png");
			this.fileTreeIcons.Images.SetKeyName(2, "RegularFile.png");
			this.fileTreeIcons.Images.SetKeyName(3, "ChangedButLockedBySomeoneElse.png");
			this.fileTreeIcons.Images.SetKeyName(4, "ChangedButNotLocked.png");
			this.fileTreeIcons.Images.SetKeyName(5, "Locked.png");
			this.fileTreeIcons.Images.SetKeyName(6, "LockedBySomeoneElse.png");
			this.fileTreeIcons.Images.SetKeyName(7, "LockedAndChanged.png");
			this.fileTreeIcons.Images.SetKeyName(8, "Added.png");
			this.fileTreeIcons.Images.SetKeyName(9, "DeletedButLockedBySomeoneElse.png");
			this.fileTreeIcons.Images.SetKeyName(10, "DeletedButNotLocked.png");
			this.fileTreeIcons.Images.SetKeyName(11, "LockedAndDeleted.png");
			// 
			// toolsButton
			// 
			this.toolsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.toolsButton.AutoSize = true;
			this.toolsButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.toolsButton.BackColor = System.Drawing.Color.Transparent;
			this.toolsButton.Depth = 0;
			this.toolsButton.Icon = null;
			this.toolsButton.Location = new System.Drawing.Point(720, 26);
			this.toolsButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.toolsButton.Name = "toolsButton";
			this.toolsButton.Primary = false;
			this.toolsButton.Size = new System.Drawing.Size(36, 36);
			this.toolsButton.TabIndex = 8;
			this.toolsButton.Text = "☰";
			this.toolsButton.UseVisualStyleBackColor = false;
			this.toolsButton.Click += new System.EventHandler(this.toolsButton_Click);
			// 
			// hamburgerMenu
			// 
			this.hamburgerMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.hamburgerMenu.Depth = 0;
			this.hamburgerMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.commitHistoryToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.logToolStripMenuItem});
			this.hamburgerMenu.MouseState = MaterialSkin.MouseState.HOVER;
			this.hamburgerMenu.Name = "hamburgerMenu";
			this.hamburgerMenu.Size = new System.Drawing.Size(181, 92);
			// 
			// settingsToolStripMenuItem
			// 
			this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
			this.settingsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.settingsToolStripMenuItem.Text = "Settings";
			this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
			// 
			// logToolStripMenuItem
			// 
			this.logToolStripMenuItem.Name = "logToolStripMenuItem";
			this.logToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.logToolStripMenuItem.Text = "Log";
			this.logToolStripMenuItem.Click += new System.EventHandler(this.logToolStripMenuItem_Click);
			// 
			// refreshButton
			// 
			this.refreshButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.refreshButton.AutoSize = true;
			this.refreshButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.refreshButton.BackColor = System.Drawing.Color.Transparent;
			this.refreshButton.Depth = 0;
			this.refreshButton.Icon = ((System.Drawing.Image)(resources.GetObject("refreshButton.Icon")));
			this.refreshButton.Location = new System.Drawing.Point(607, 26);
			this.refreshButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.refreshButton.Name = "refreshButton";
			this.refreshButton.Primary = false;
			this.refreshButton.Size = new System.Drawing.Size(107, 36);
			this.refreshButton.TabIndex = 11;
			this.refreshButton.Text = "Refresh";
			this.refreshButton.UseVisualStyleBackColor = false;
			this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
			// 
			// workspaceContextMenu
			// 
			this.workspaceContextMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.workspaceContextMenu.Depth = 0;
			this.workspaceContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkoutToolStripMenuItem});
			this.workspaceContextMenu.MouseState = MaterialSkin.MouseState.HOVER;
			this.workspaceContextMenu.Name = "workspaceContextMenu";
			this.workspaceContextMenu.Size = new System.Drawing.Size(126, 26);
			// 
			// checkoutToolStripMenuItem
			// 
			this.checkoutToolStripMenuItem.Name = "checkoutToolStripMenuItem";
			this.checkoutToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
			this.checkoutToolStripMenuItem.Text = "Checkout";
			this.checkoutToolStripMenuItem.Click += new System.EventHandler(this.checkoutToolStripMenuItem_Click);
			// 
			// changesContextMenu
			// 
			this.changesContextMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.changesContextMenu.Depth = 0;
			this.changesContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unlockFile});
			this.changesContextMenu.MouseState = MaterialSkin.MouseState.HOVER;
			this.changesContextMenu.Name = "workspaceRightClick";
			this.changesContextMenu.Size = new System.Drawing.Size(112, 26);
			// 
			// unlockFile
			// 
			this.unlockFile.Name = "unlockFile";
			this.unlockFile.Size = new System.Drawing.Size(111, 22);
			this.unlockFile.Text = "Unlock";
			this.unlockFile.Click += new System.EventHandler(this.unlockFile_Click);
			// 
			// progressBar
			// 
			this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.progressBar.Depth = 0;
			this.progressBar.Location = new System.Drawing.Point(12, 495);
			this.progressBar.MouseState = MaterialSkin.MouseState.HOVER;
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(744, 5);
			this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.progressBar.TabIndex = 14;
			this.progressBar.Value = 100;
			// 
			// changedFiles
			// 
			this.changedFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.changedFiles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.changedFiles.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.changedFiles.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.changedFiles.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.changedFiles.FullRowSelect = true;
			this.changedFiles.ImageKey = "RegularFile.png";
			this.changedFiles.ImageList = this.fileTreeIcons;
			this.changedFiles.ItemHeight = 28;
			this.changedFiles.Location = new System.Drawing.Point(391, 93);
			this.changedFiles.Name = "changedFiles";
			this.changedFiles.SelectedImageIndex = 0;
			this.changedFiles.Size = new System.Drawing.Size(365, 385);
			this.changedFiles.TabIndex = 15;
			this.changedFiles.MouseClick += new System.Windows.Forms.MouseEventHandler(this.checkedOutFiles_MouseClick);
			// 
			// commitHistoryToolStripMenuItem
			// 
			this.commitHistoryToolStripMenuItem.Name = "commitHistoryToolStripMenuItem";
			this.commitHistoryToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.commitHistoryToolStripMenuItem.Text = "Commit history";
			this.commitHistoryToolStripMenuItem.Click += new System.EventHandler(this.commitHistoryToolStripMenuItem_Click);
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.ClientSize = new System.Drawing.Size(768, 512);
			this.Controls.Add(this.changedFiles);
			this.Controls.Add(this.progressBar);
			this.Controls.Add(this.refreshButton);
			this.Controls.Add(this.toolsButton);
			this.Controls.Add(this.fileTree);
			this.Controls.Add(this.changesetLabel);
			this.Controls.Add(this.divider);
			this.Controls.Add(this.fileTreeLabel);
			this.Controls.Add(this.pushButton);
			this.Controls.Add(this.pullButton);
			this.MinimumSize = new System.Drawing.Size(512, 320);
			this.Name = "MainWindow";
			this.Text = "PHILISTINE";
			this.Resize += new System.EventHandler(this.MainWindow_Resize);
			this.hamburgerMenu.ResumeLayout(false);
			this.workspaceContextMenu.ResumeLayout(false);
			this.changesContextMenu.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private MaterialSkin.Controls.MaterialRaisedButton pushButton;
		private MaterialSkin.Controls.MaterialLabel fileTreeLabel;
		private MaterialSkin.Controls.MaterialDivider divider;
		private MaterialSkin.Controls.MaterialLabel changesetLabel;
		private System.Windows.Forms.TreeView fileTree;
		private MaterialSkin.Controls.MaterialRaisedButton toolsButton;
		private MaterialSkin.Controls.MaterialContextMenuStrip hamburgerMenu;
		private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem logToolStripMenuItem;
		private MaterialSkin.Controls.MaterialRaisedButton refreshButton;
		private MaterialSkin.Controls.MaterialContextMenuStrip workspaceContextMenu;
		private System.Windows.Forms.ToolStripMenuItem checkoutToolStripMenuItem;
		private MaterialSkin.Controls.MaterialContextMenuStrip changesContextMenu;
		private System.Windows.Forms.ToolStripMenuItem unlockFile;
		private MaterialSkin.Controls.MaterialProgressBar progressBar;
		private System.Windows.Forms.ImageList fileTreeIcons;
		private System.Windows.Forms.TreeView changedFiles;
		private MaterialSkin.Controls.MaterialRaisedButton pullButton;
		private System.Windows.Forms.ToolStripMenuItem commitHistoryToolStripMenuItem;
	}
}