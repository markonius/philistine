﻿namespace Philistine
{
	partial class PushWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.commitMessageBox = new System.Windows.Forms.TextBox();
			this.pushButton = new MaterialSkin.Controls.MaterialRaisedButton();
			this.note = new MaterialSkin.Controls.MaterialLabel();
			this.helpButton = new MaterialSkin.Controls.MaterialFlatButton();
			this.SuspendLayout();
			// 
			// commitMessageBox
			// 
			this.commitMessageBox.AcceptsReturn = true;
			this.commitMessageBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.commitMessageBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.commitMessageBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.commitMessageBox.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.commitMessageBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.commitMessageBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.commitMessageBox.Location = new System.Drawing.Point(12, 78);
			this.commitMessageBox.Multiline = true;
			this.commitMessageBox.Name = "commitMessageBox";
			this.commitMessageBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.commitMessageBox.Size = new System.Drawing.Size(598, 273);
			this.commitMessageBox.TabIndex = 0;
			// 
			// pushButton
			// 
			this.pushButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pushButton.AutoSize = true;
			this.pushButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.pushButton.BackColor = System.Drawing.Color.Transparent;
			this.pushButton.Depth = 0;
			this.pushButton.Icon = null;
			this.pushButton.Location = new System.Drawing.Point(553, 26);
			this.pushButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.pushButton.Name = "pushButton";
			this.pushButton.Primary = true;
			this.pushButton.Size = new System.Drawing.Size(57, 36);
			this.pushButton.TabIndex = 1;
			this.pushButton.Text = "Push";
			this.pushButton.UseVisualStyleBackColor = false;
			this.pushButton.Click += new System.EventHandler(this.pushButton_Click);
			// 
			// note
			// 
			this.note.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.note.AutoSize = true;
			this.note.Depth = 0;
			this.note.Font = new System.Drawing.Font("Roboto", 11F);
			this.note.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.note.Location = new System.Drawing.Point(12, 368);
			this.note.MouseState = MaterialSkin.MouseState.HOVER;
			this.note.Name = "note";
			this.note.Size = new System.Drawing.Size(291, 19);
			this.note.TabIndex = 2;
			this.note.Text = "Remember to review your work thoroughly";
			// 
			// helpButton
			// 
			this.helpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.helpButton.AutoSize = true;
			this.helpButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.helpButton.Depth = 0;
			this.helpButton.Icon = null;
			this.helpButton.Location = new System.Drawing.Point(309, 360);
			this.helpButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
			this.helpButton.MouseState = MaterialSkin.MouseState.HOVER;
			this.helpButton.Name = "helpButton";
			this.helpButton.Primary = false;
			this.helpButton.Size = new System.Drawing.Size(301, 36);
			this.helpButton.TabIndex = 3;
			this.helpButton.Text = "Click to see a sample commit message";
			this.helpButton.UseVisualStyleBackColor = true;
			this.helpButton.Click += new System.EventHandler(this.helpButton_Click);
			// 
			// PushWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(622, 405);
			this.Controls.Add(this.helpButton);
			this.Controls.Add(this.note);
			this.Controls.Add(this.pushButton);
			this.Controls.Add(this.commitMessageBox);
			this.MinimumSize = new System.Drawing.Size(622, 256);
			this.Name = "PushWindow";
			this.Text = "Enter commit message";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PushWindow_FormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox commitMessageBox;
		private MaterialSkin.Controls.MaterialRaisedButton pushButton;
		private MaterialSkin.Controls.MaterialLabel note;
		private MaterialSkin.Controls.MaterialFlatButton helpButton;
	}
}