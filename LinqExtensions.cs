﻿using System;
using System.Collections.Generic;

namespace Philistine
{
	public static class LinqExtensions
	{
		public static IEnumerable<T> FilterAndSelectRecursive<T>(
			this IEnumerable<T> source,
			Func<T, bool> filter,
			Func<T, IEnumerable<T>> selector)
		{
			foreach (T t in source)
			{
				if (filter(t))
					yield return t;
				foreach (T t1 in FilterAndSelectRecursive(selector(t), filter, selector))
					yield return t1;
			}
		}
	}
}
